# tengine-docker

#### 介绍
制作tengine docker 镜像，国内源，编译速度更快

| tengine版本  | 2.3.2 |
|-------------|-------|
| alpine 版本  | 3.12  |


如何制作：

1. git clone https://gitee.com/castdlg/tengine-docker.git 到本地

2. 进入tengine-docker文件夹执行`./build-docker-image.sh` 
这里可能会提示没有权限执行，`chmod 777 build-docker-image.sh`

3.等待构建tengine 镜像，采用清华大学的源，下载速度更快

如何启动tengine docker容器？

```
docker run -d -v tengine-docker/example.com.conf:/etc/nginx/conf.d/example.com.conf \
    -p "8083:8083" tengine:1.0
```
注意：
-v  **tengine-docker/example.com.conf** :/etc/nginx/conf.d/example.com.conf
-v 需要本机example.com.conf文件的绝对路径



基于docker-compose 版本的跳转到 https://gitee.com/castdlg/tengine-docker-compose.git



